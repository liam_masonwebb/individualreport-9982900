
[//]: <> (Individual Journal)
<span style="color:coral; font-size:19pt"> Tools </span>



##4/09/17
Our group discussed what communication method we will use. We drew conclusions that not everyone has Facebook and some of those who had Facebook did not use it regularly so we decided to use Slack as our primary form of communication as we all use slack and all have the mobile apps on our phone thus allowing us to get direct messages instantly. We decided facebook and email will be used as a backup because not all of us are on Facebook all the time or check our emails regularly so it would not be suitable.


##6/09/17
We decided to use google docs to write and share reports with group members. Google docs is the best File sharing platform we found because it allows us to edit documents and information with different people interacting and allows us to comment on each other’s work. We also discussed using git and markdown files however we all were confident about using google docs.




##07/09/17


We further discussed how to share our ideas and we brainstormed how to share our ideas remotely and we came across trello and decided it will be used to share ideas for the program. Also we decided to do all our wireframes on draw-io . We decided to use Draw-io to make our wireframes because it is easy to use and provides accurate representation of our website we will develop. Bitbucket will be used to host our program and reports. We decided to use Bitbucket Rather than GitHub because it is easer for us to use as we are all used to the platform and are rather confident with it
